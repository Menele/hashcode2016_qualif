#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <list>
#include <time.h>
#include <string>
#include <Windows.h>
using namespace std;

vector<long> ProductWeights;

struct Coord
{
	long row;
	long col;
};

long getDistance(const Coord &c1, const Coord &c2)
{
	return ceil(sqrt((c1.row - c2.row)*(c1.row - c2.row) + (c1.col - c2.col)*(c1.col - c2.col)));
}

long getDistanceSq(const Coord &c1, const Coord &c2)
{
	return ceil(((c1.row - c2.row)*(c1.row - c2.row) + (c1.col - c2.col)*(c1.col - c2.col)));
}


class Order
{
public:
	long id;
	Coord cell;
	bool done = false;

	list<long> items;
	long totalWeight = 0;
	long cost;

	// list of other orders by distance
	list<long> ordersByDist;

	bool needProduct(long itemId)
	{
		for (long p : items)
		{
			if (p == itemId)
				return true;
		}
		return false;
	}

	// remove delivered items, return remaining quantity
	long deliverItem(long prodId, long maxQty)
	{
		for (auto it = items.begin(); it != items.end(); )
		{
			if (maxQty == 0)
				return 0;
			if ((*it) == prodId)
			{
				auto itDel = it;
				++it;
				items.erase(itDel);

				--maxQty;
			}
			else
				++it;
		}

		// if all items delivered, set done to true
		if (items.empty())
			done = true;

		return maxQty;
	}

	// cost: estimation of the number of turns to complete command
	void computeCost()
	{


		//cost += items.size();//+rand() % 1000;
	}

	void addAndSortOtherOrders();

};
//list<Order> Orders;
// keep all orders by id
vector<Order> AllOrders;
// list of orders to process
list<long> Orders;


struct cmpOrderCell
{
	Coord cell;
	cmpOrderCell(const Coord &c) : cell(c) {}

	bool operator()(const Order &o1, const Order &o2)
	{
		const long d1 = getDistanceSq(o1.cell, cell);
		const long d2 = getDistanceSq(o2.cell, cell);
		return d1 < d2;
	}
};

struct cmpOrderIdCellDist
{
	Coord cell;
	cmpOrderIdCellDist(const Coord &c) : cell(c) {}

	bool operator()(long o1, long o2)
	{
		const long d1 = getDistance(AllOrders[o1].cell, cell);
		const long d2 = getDistance(AllOrders[o2].cell, cell);

		return d1 < d2;
	}
};

long factWeight = 2;
long randw = 1;
long MAX_LOAD = 200;

struct cmpOrderIdCell
{
	Coord cell;
	cmpOrderIdCell(const Coord &c) : cell(c) {}

	bool operator()(long o1, long o2)
	{
		const long d1 = getDistance(AllOrders[o1].cell, cell) + AllOrders[o1].totalWeight *factWeight +rand() % randw;//+AllOrders[o1].items.size() * 1000;
		const long d2 = getDistance(AllOrders[o2].cell, cell) + AllOrders[o2].totalWeight *factWeight +rand() % randw;;//+AllOrders[o1].items.size() * 1000;
		//const long d1 = getDistance(AllOrders[o1].cell, cell) * (1+ AllOrders[o1].totalWeight/MAX_LOAD )  + rand() % randw;
		//const long d2 = getDistance(AllOrders[o2].cell, cell) * (1 + AllOrders[o1].totalWeight / MAX_LOAD)  + rand() % randw;

		return d1 < d2;
	}
};

void Order::addAndSortOtherOrders()
{
	long n = AllOrders.size();
	for (long i = 0; i < n; ++i)
	{
		if (i != this->id)
		{
			ordersByDist.push_back(i);
		}
	}
	ordersByDist.sort(cmpOrderIdCellDist(cell));
}




struct cmpOrder
{
	bool operator()(const Order &o1, const Order &o2)
	{
		return o1.cost < o2.cost;
	}
};

struct cmpOrderId
{
	bool operator()(long o1, long o2)
	{
		return AllOrders[o1].cost < AllOrders[o1].cost;
	}
};

class Warehouse
{
public:
	Coord cell;
	long totalCount;
	vector<long> ProductCount;

	void sortOrdersByDistance()
	{
		// add all orders
		sortedOrderIds.clear();
		const long n = Orders.size();
		for (long i = 0; i < n; ++i)
		{
			sortedOrderIds.push_back(i);
		}

		sortedOrderIds.sort(cmpOrderIdCellDist(cell));
	}

	void removeOrder(long id)
	{
		list<long>::iterator itOrder = std::find(sortedOrderIds.begin(), sortedOrderIds.end(), id);
		if (itOrder != sortedOrderIds.end())
			sortedOrderIds.erase(itOrder);
	}

	// orders sorted from closest to furthest from this warehouse
	list<long> sortedOrderIds;
};

vector<Warehouse> Warehouses;

Warehouse *getClosestWar(Coord &cell, long p)
{
	long dMin = INT_MAX;
	Warehouse *closest = nullptr;
	for (Warehouse &w : Warehouses)
	{
		long d = getDistanceSq(cell, w.cell);
		if (d < dMin)
		{
			dMin = d;
			closest = &w;
		}
	}
	return closest;
}

/*Warehouse *getClosestProductWare(const list<long> &prod, Coord &c, list<long> &remaining, long &takenP)
{

}*/

class Drone
{
public:
	long id;
	Coord cell;
	long time = 0;
	//list< pair<long, long> > products; // pair<ID, count>
	long loadedProductId = 0;
	long loadedProductQty = 0;
	long curLoad = 0;
	vector<long> products; // qty by product
};

struct DroneCmp
{
	bool operator()(const Drone &d1, const Drone &d2)
	{
		return d1.time < d2.time;
	}
};

list<Drone> Drones;

enum CType
{
	LOAD = 0,
	UNLOAD,
	DELIVER,
	WAIT,
};

class Command
{
public:
	int droneId;
	CType type;

	virtual string toString() const = 0;

};



class CommandLoad : public Command
{
public:
	long wareId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " L " << wareId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandUnload : public Command
{
public:
	long wareId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " U " << wareId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandDeliver : public Command
{
public:
	long orderId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " D " << orderId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandWait : public Command
{
public:
	long nbTurns;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " W " << nbTurns;
		return ss.str();
	}

};






class DeliverySystem
{
public:
	long rows;
	long columns;
	long nbDrones;

	long deadline;
	long maxLoad;
	long nbRemainingOrders;
	long finalScore = 0;
	long getDeliveryScore(long time)
	{
		return ceil(100 * double(deadline - time) / deadline);
	}

	list<Command*> deliver()
	{

		list<Command*> commands;

		// iter while we have time left

		long scoreTotal = 0;

		while (nbRemainingOrders && !Drones.empty())
		{

			Drones.sort(DroneCmp());

			// take first available drone
			Drone &selDrone = Drones.front();

	//		cout << "Remaining orders = " << nbRemainingOrders << endl;
		//	cout << "Drone id=" << selDrone.id << " time=" << selDrone.time << " loaded P="<<selDrone.loadedProductId <<" Qty="<<selDrone.loadedProductQty<<endl;

			// if drone is empty send it to a warehouse, ortherwise send it to closest order needing loaded product
			if (selDrone.loadedProductQty > 0)
			{

				long orderId = -1;
				long mindist = LONG_MAX;
				for (int o = 0; o < AllOrders.size(); ++o)
				{
					if (!AllOrders[o].done && AllOrders[o].needProduct(selDrone.loadedProductId))
					{
						long dis = getDistance(selDrone.cell, AllOrders[o].cell);
						if (dis < mindist)
						{
							mindist = dis;
							orderId = o;
						}
					}
				}

				if (orderId == -1)
				{
					//    cout << "No order seems to need product " << selDrone.loadedProductId << " Unload it to warehouse 0" << endl;
					CommandUnload *cmd = new CommandUnload();
					cmd->droneId = selDrone.id;
					cmd->nb = selDrone.loadedProductQty;
					cmd->wareId = 0;
					cmd->prod = selDrone.loadedProductId;

					commands.push_back(cmd);

					long time = selDrone.time;
					time += 1 + getDistance(selDrone.cell, Warehouses[0].cell);
					selDrone.time = time;
					selDrone.cell = Warehouses[0].cell;
					selDrone.loadedProductQty = 0;
					selDrone.loadedProductId = 0;
					continue;
				}

				// check time to get there
				long time = selDrone.time;
				time += mindist;
				time += 1; // to deliver
				if (time >= deadline)
				{
					// delete bot, start again
					Drones.pop_front();
					continue;
				}

				// check needed qty
				long qtyRemaining = AllOrders[orderId].deliverItem(selDrone.loadedProductId, selDrone.loadedProductQty);
				long delivered = selDrone.loadedProductQty - qtyRemaining;
				selDrone.loadedProductQty = qtyRemaining;
				selDrone.time = time;
				selDrone.cell = AllOrders[orderId].cell;

				if (AllOrders[orderId].done) {
					--nbRemainingOrders;
					scoreTotal += getDeliveryScore(selDrone.time);
				}

				// add command
				CommandDeliver *cmd = new CommandDeliver();
				cmd->droneId = selDrone.id;
				cmd->nb = delivered;
				cmd->orderId = orderId;
				cmd->prod = selDrone.loadedProductId;

				commands.push_back(cmd);

				continue;
			}

			// send it to the closest non-empty warehouse, load items and dispatch them to closest orders for this item
			long minDist = 10000;
			long selW = 0;
			for (int wi = 0; wi < Warehouses.size(); ++wi)
			{
				Warehouse &w = Warehouses[wi];
				if (w.totalCount > 0)
				{
					long dist = getDistance(w.cell, selDrone.cell);

					if (dist < minDist)
					{
						minDist = dist;
						selW = wi;
					}
				}
			}
			Warehouse &w = Warehouses[selW];

			bool ok = false;
			long chosenProduct = -1;
			long selOrderOid = -1;
			unordered_map<long, long> loadedProd;
			list<long> selOrders;
			{
				// get order in the right...order and check if this warehouse have 
				// some of the right products, chose the one for which we can load the max number of same item
				long maxItemConsidered = 2;
				long curItem = 0;
				long curLoad = 0;
				list<long> *curList = &w.sortedOrderIds;
				list<long>::iterator nextItem = w.sortedOrderIds.begin();
				//for (long oid : w.sortedOrderIds)
				while (nextItem != curList->end())
				{
					long oid = *nextItem;
					auto ititem = std::find(selOrders.begin(), selOrders.end(), oid);
					if (ititem != selOrders.end())
					{
						++nextItem;
						continue;
					}
					Order &selOrder = AllOrders[oid];
					if (!selOrder.done)
					{
						
						bool oneTaken = false;
						for (auto itItems = selOrder.items.begin(); itItems != selOrder.items.end(); )
						{
							bool taken = false;
							long p = *itItems;
							if (w.ProductCount[p] > 0)
							{
								if (curLoad + ProductWeights[p] <= maxLoad)
								{
									oneTaken = true;
									if (loadedProd.find(p) == loadedProd.end())
									{
										loadedProd[p] = 1;
									}
									else
									{
										long cp = loadedProd[p];
										loadedProd.erase(p);
										loadedProd[p] = cp + 1;
									}
									--w.ProductCount[p];
									--w.totalCount;
									curLoad += ProductWeights[p];
							//		auto itPrev = itItems;
						//			++itItems;
									taken = true;
//									selOrder.items.erase(itPrev);
									curList = &selOrder.ordersByDist;
									nextItem = curList->begin();
								}
							}

					//		if (!taken)
							if (!taken)
							{
								++nextItem;
							}
							++itItems;
						}
						if (oneTaken) 
						{
							++curItem;
							selOrders.push_back(oid);
						}
						/*for (long p : selOrder.items)
						{
							if (w.ProductCount[p] > 0)
							{
								minWeight = ProductWeights[p];
								chosenProduct = p;
								selOrderOid = oid;
								ok = true;
								selOrders.push_back(oid);
							}
						}*/
					}
					else
					{
						++nextItem;
					}
					if (maxItemConsidered<=curItem || curLoad == maxLoad)
						break;
				}
			}
			if (selOrders.empty())
			{
				// no useful items in this warehouse, remove it and retry
				w.totalCount = 0;
				//Drones.pop_front();
				continue;
			}

			// check possibility
			long time = selDrone.time;
			time += getDistance(selDrone.cell, w.cell);
			//time += 2; // load and deliver command
			time += getDistance(w.cell, AllOrders[selOrders.front()].cell);
			if (time >= deadline)
			{
				//cout << "Drone id=" << selDrone.id << " would reach time limit for command, with time=" << selDrone.time << endl;
				// erase drone
				Drones.pop_front();
				// try next one
				continue;
			}
	
			// load command
			for (auto it = loadedProd.begin(); it != loadedProd.end(); ++it)
			{
				CommandLoad *cload = new CommandLoad();
				cload->droneId = selDrone.id;
				cload->wareId = selW;
				cload->prod = it->first;
				cload->nb = it->second;
				commands.push_back(cload);
				++time;
			}

			//deliver commands
			long prevoid = selOrders.front();
			for (long oid : selOrders)
			{
				if (oid != prevoid)
				{
					time += getDistance(AllOrders[oid].cell, AllOrders[prevoid].cell);
					prevoid = oid;
				}
				unordered_map<long,long> remainingProd;
				for (auto it = loadedProd.begin(); it != loadedProd.end(); ++it)
				{
					long prodQty = it->second;
					long remainingQty = AllOrders[oid].deliverItem(it->first, prodQty);
					long deliveredQty = prodQty - remainingQty;
					if (deliveredQty>0)
					{
						CommandDeliver *cd = new CommandDeliver();
						cd->droneId = selDrone.id;
						cd->orderId = oid;
						cd->prod = it->first;
						cd->nb = deliveredQty;
						commands.push_back(cd);
						++time;
					}
					if (remainingQty > 0)
					{
						remainingProd.insert(make_pair(it->first, remainingQty));
					}
				}
				loadedProd = remainingProd;
				//update order
				if (AllOrders[oid].items.empty())
				{
					scoreTotal += getDeliveryScore(time);
					AllOrders[oid].done = true;
					--nbRemainingOrders;
				}
			}

			

			// update Drone
			selDrone.time = time;
			selDrone.cell = AllOrders[selOrders.back()].cell;
			selDrone.loadedProductQty = 0;

		}
		
		//cout << "Score ="<<scoreTotal << endl;
		finalScore = scoreTotal;

		return commands;
	}

};


int main(int argc, char** argv)
{

	

	long bestScore = 0;
	srand(time(NULL));
	int nbIter = 100;
	while (nbIter)
	{
		std::string inputFile("C:\\Users\\David\\Downloads\\mother_of_all_warehouses.in");
		std::string outputFile("C:\\Users\\David\\documents\\visual studio 2015\\Projects\\Hashcode_2016_qualif\\Hashcode_2016_qualif\\mother_of_all_warehouses.out");

		--nbIter;
	
		randw =1+ rand() % 500;
		factWeight = rand() % 100;
		ifstream in;
		in.open(inputFile.c_str());
		

		/*	number of rows in the area of the simulation(1 ? number of rows ? 10000)
		? number of columns in the area of the simulation(1 ? number of columns ? 10000)
		? D � number of drones available(1 ? D ? 1000)
		? deadline of the simulation(1 ? deadline of the simulation ? 1000000)
		? maximum load of a drone(1 ? maximum load of a drone ? 10000)
		*/
		DeliverySystem ds;
		in >> ds.rows;
		in >> ds.columns;
		in >> ds.nbDrones;
		in >> ds.deadline;
		in >> ds.maxLoad;
		MAX_LOAD = ds.maxLoad;

		//weight
		long nbProducts;
		in >> nbProducts;
		ProductWeights.clear();
		ProductWeights.resize(nbProducts);
		for (long p = 0; p < nbProducts; ++p) {
			in >> ProductWeights[p];
		}

		// Warehouses
		long nbW;
		in >> nbW;
		Warehouses.clear();
		Warehouses.resize(nbW);

		for (int wc = 0; wc < nbW; ++wc)
		{
			long r, c;
			in >> r;
			in >> c;

			Warehouses[wc].cell.row = r;
			Warehouses[wc].cell.col = c;
			Warehouses[wc].totalCount = 0;
			Warehouses[wc].ProductCount.resize(nbProducts, 0);

			// product counts
			for (int p = 0; p < nbProducts; ++p)
			{
				long count;
				in >> count;
				Warehouses[wc].totalCount += count;
				Warehouses[wc].ProductCount[p] = count;
			}
		}

		// create drones, all in warehouse 0
		Drones.clear();
		for (int d = 0; d < ds.nbDrones; ++d)
		{
			Drone dr;
			dr.id = d;
			dr.cell = Warehouses[0].cell;
			dr.time = 0;
			dr.curLoad = 0;
			dr.products.resize(nbProducts, 0);
			Drones.push_back(dr);
		}


		// order
		long nbOrders;
		in >> nbOrders;
		AllOrders.clear();
		Orders.clear();
		AllOrders.resize(nbOrders);
		for (int n = 0; n < nbOrders; ++n)
		{
			Order order;
			order.id = n;
			in >> order.cell.row;
			in >> order.cell.col;

			long nbp;
			in >> nbp;

			for (int p = 0; p < nbp; ++p)
			{
				long pin;
				in >> pin;
				order.items.push_back(pin);
				order.totalWeight += ProductWeights[pin];
			}

			AllOrders[n] = order;
			//Orders.push_back(order);
			Orders.push_back(n);
		}

		for (auto &o : AllOrders)
		{
			o.addAndSortOtherOrders();
		}
		
		for (auto &w : Warehouses)
		{
			w.sortOrdersByDistance();
		}

		ds.nbRemainingOrders = nbOrders;
		list<Command*> commands = ds.deliver();
		
		if (ds.finalScore > bestScore)
		{
			cout << "New best score=" << ds.finalScore << " iterremaining = " << nbIter << endl;
			bestScore = ds.finalScore;
			ofstream out;
			out.open(outputFile);
			out << commands.size() << endl;
			
			for (Command *c : commands)
			{
				out << c->toString() << "\n";
				delete c;
			}
			out.close();
		}
		in.close();
		
	}
	cout << "ENDED bestScore=" << bestScore<< endl;
	Sleep(15000);
//	long wait;
//	cin >> wait;

}
