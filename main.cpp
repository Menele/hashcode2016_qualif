#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <string>
#include <Windows.h>
using namespace std;

vector<long> ProductWeights;



struct Coord
{
	long row;
	long col;
};


long getDistance(const Coord &c1, const Coord &c2)
{
	return ceil(sqrt((c1.row - c2.row)*(c1.row - c2.row) + (c1.col - c2.col)*(c1.col - c2.col)));
}


long getDistanceSq(const Coord &c1, const Coord &c2)
{
	return ceil(((c1.row - c2.row)*(c1.row - c2.row) + (c1.col - c2.col)*(c1.col - c2.col)));
}

class Warehouse
{
public:
	Coord cell;
	vector<long> ProductCount;
};

vector<Warehouse> Warehouses;



Warehouse *getClosestWar(Coord &cell, long p)
{
	long dMin = INT_MAX;
	Warehouse *closest = nullptr;
	for (Warehouse &w : Warehouses)
	{
		long d = getDistanceSq(cell, w.cell);
		if (d < dMin)
		{
			dMin = d;
			closest = &w;
		}
	}
	return closest;
}

/*Warehouse *getClosestProductWare(const list<long> &prod, Coord &c, list<long> &remaining, long &takenP)
{

}*/

class Drone
{
public:
	long id;
	Coord cell;
	long time;
	list< pair<long, long> > products; // pair<ID, count>
};

struct DroneCmp
{
	bool operator()(const Drone &d1, const Drone &d2)
	{
		return d1.time < d2.time;
	}
};

list<Drone> Drones;

enum CType
{
	LOAD =0,
	UNLOAD,
	DELIVER,
	WAIT,
};

class Command
{
public:
	int droneId;
	CType type;

	virtual string toString() const = 0;

};



class CommandLoad : public Command
{
public:
	long wareId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " L " << wareId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandUnload : public Command
{
public:
	long wareId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " U " << wareId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandDeliver : public Command
{
public:
	long orderId;
	long prod;
	long nb;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " D " << orderId << " " << prod << " " << nb;
		return ss.str();
	}

};

class CommandWait : public Command
{
public:
	long nbTurns;

	virtual string toString() const
	{
		stringstream ss;
		ss << droneId << " W " << nbTurns;
		return ss.str();
	}

};


class Order
{
public:
	long id;
	Coord cell;

	list<int> items;

	long cost;

	

	// cost: estimation of the number of turns to complete command
	void computeCost()
	{
		int cost = 0;

		for (int p : items)
		{
			long mindis = INT_MAX;

			//Warehouse *bestWa = nullptr;
			for (auto w : Warehouses)
			{
				if (w.ProductCount[p] > 0)
				{
					long d = getDistance(cell, w.cell);
					if (d < mindis)
					{
						mindis = d;
					}
				}
			}


			// TODO : a changer pour faire un seul drone ?
			if (mindis + 1 > cost)
				cost = mindis +1;
		}

		cost += items.size();//+rand() % 1000;
	}

		double evalDroneScore(const Drone &d)
		{
			double score = d.time;
			for (auto w : Warehouses)
			{
				long dToW = getDistance(w.cell, d.cell);

				// score of warehouse = nb items possible to deliver
				long itemAvailable = 0;
				for (int p : items)
				{
					if (w.ProductCount[p] > 0)
					{
						++itemAvailable;
					}
				}

				if (itemAvailable > 0)
				{
					score = getDistance(w.cell, cell);
					score += 1 + dToW ;// / itemAvailable;
				}
			}

			return score;
		}
	

};
list<Order> Orders;


struct cmpOrder
{
	bool operator()(const Order &o1, const Order &o2)
	{
		return o1.cost < o2.cost;
	}
};


class DeliverySystem
{
public:
	long rows;
	long columns;
	long nbDrones;

	long deadline;
	long maxLoad;
	
	long getDeliveryScore(long time)
	{
		return ceil(100 * double(deadline - time) / deadline);
	}

	list<Command*> deliver()
	{

		list<Command*> commands;

		// iter while we have time left
		
		long scoreTotal = 0;

		while (!Orders.empty())
		{
			for (auto o : Orders)
			{
				o.computeCost();
			}
			Orders.sort(cmpOrder());
			
			// take first order
			Order &o = Orders.front();

			// envoie drone le plus pres sur la premiere commande possible
			Drone *selDrone = nullptr;
			double minScore = 100000;
			for (Drone &d : Drones)
			{
				double s = o.evalDroneScore(d);
				if (s < minScore && d.time+s < deadline )
				{
					minScore = s;
					selDrone = &d;
				}
			}

			// drone choisi on l'envoie au warehouse choisi, on charge tout ce que l'on peut, on l'envoie sur la commande
			// on d�charge, et on recommence !

		//	cout << "Order id=" << o.id << " Drone id=" << selDrone->id << "  time=" << selDrone->time << endl;

			// si le temps est d�pass� pour ce drone -> fin de l'algo
			long minDist = 10000;
			long selW = 0;
			for (int wi = 0; wi < Warehouses.size(); ++wi)
			{
				Warehouse &w = Warehouses[wi];
				long dist = getDistance(w.cell, selDrone->cell);
				dist += getDistance(w.cell, o.cell);

				// score of warehouse = nb items possible to deliver
				long itemAvailable = 0;
				for (int p : o.items)
				{
					if (w.ProductCount[p] > 0)
					{
						++itemAvailable;
					}
				}

				if (itemAvailable > 0)
				{
					if (dist < minDist) {
						minDist = dist;
						selW = wi;
					}
				}
			}

			Warehouse &w = Warehouses[selW];
			// compute time

			// get loaded items
			map<long, long> loadedProd;
			long curLoad = 0;

			for (auto itItems = o.items.begin(); itItems != o.items.end(); )
			{
				bool taken = false;
				long p = *itItems;
				if (w.ProductCount[p] > 0)
				{
					if (curLoad + ProductWeights[p] <= maxLoad)
					{
						if (loadedProd.find(p) == loadedProd.end())
						{
							loadedProd[p] = 1;
						}
						else
						{
							long cp = loadedProd[p];
							loadedProd.erase(p);
							loadedProd[p] = cp + 1;
						}
						w.ProductCount[p] = w.ProductCount[p] - 1;
						curLoad += ProductWeights[p];
						auto itPrev = itItems;
						++itItems;
						taken = true;
						o.items.erase(itPrev);
					}
				}

				if (!taken)
					++itItems;
			}
			

			list<Command*> commandsForDrone;
			long endTime = selDrone->time;
			endTime += minDist;
			endTime += getDistance(w.cell, o.cell);
			// load command
			for (auto it = loadedProd.begin(); it != loadedProd.end(); ++it)
			{
				CommandLoad *cload = new CommandLoad();
				cload->droneId = selDrone->id;
				cload->wareId = selW;
				cload->prod = it->first;
				cload->nb = it->second;
				commandsForDrone.push_back(cload);
			}
						
			//deliver command
			for (auto it = loadedProd.begin(); it != loadedProd.end(); ++it)
			{
				CommandDeliver *cd = new CommandDeliver();
				cd->droneId = selDrone->id;
				cd->orderId = o.id;
				cd->prod = it->first;
				cd->nb = it->second;
				commandsForDrone.push_back(cd);
			}
			
			endTime += commandsForDrone.size();

			/*if (endTime >= deadline)
			{
				// END OF ALGO
				break;
			}*/

			if (o.items.empty())
			{
				scoreTotal += getDeliveryScore(endTime);
				commands.insert(commands.end(), commandsForDrone.begin(), commandsForDrone.end());

				selDrone->time = endTime;
				selDrone->cell = o.cell;
				Drones.sort(DroneCmp());
				Orders.pop_front();
			}
			else if (endTime >= deadline)
			{
				Orders.pop_front();
			}
			else
			{
				commands.insert(commands.end(), commandsForDrone.begin(), commandsForDrone.end());

				selDrone->time = endTime;
				selDrone->cell = o.cell;
			}
			
		}


		cout << scoreTotal << endl;

		return commands;
	}

};





int main(int argc, char** argv)
{
	cout << "test" << endl;
	std::string inputFile("C:\\Users\\David\\Downloads\\mother_of_all_warehouses.in");
	std::string outputFile("c:\\Users\\David\\documents\\visual studio 2015\\Projects\\Hashcode_2016_qualif\\Hashcode_2016_qualif\\mother_of_all_warehouses.out");

	ofstream out;
	ifstream in;

	in.open(inputFile.c_str());
	out.open(outputFile.c_str());

/*	number of rows in the area of the simulation(1 ? number of rows ? 10000)
		? number of columns in the area of the simulation(1 ? number of columns ? 10000)
		? D � number of drones available(1 ? D ? 1000)
		? deadline of the simulation(1 ? deadline of the simulation ? 1000000)
		? maximum load of a drone(1 ? maximum load of a drone ? 10000)
		*/
	DeliverySystem ds;
	in >> ds.rows;
	in >> ds.columns;
	in >> ds.nbDrones;
	in >> ds.deadline;
	in >> ds.maxLoad;

	//weight
	long nbProducts;
	in >> nbProducts;

	ProductWeights.resize(nbProducts);
	for (long p = 0; p < nbProducts; ++p) {
		in >> ProductWeights[p];
	}

	// Warehouses
	long nbW;
	in >> nbW;
	Warehouses.resize(nbW);

	for (int wc = 0; wc < nbW; ++wc)
	{
		long r, c;
		in >> r;
		in >> c;

		Warehouses[wc].cell.row = r;
		Warehouses[wc].cell.col = c;

		Warehouses[wc].ProductCount.resize(nbProducts, 0);

		// product counts
		for (int p = 0; p < nbProducts; ++p)
		{
			long count;
			in >> count;
			Warehouses[wc].ProductCount[p] = count;
		}
	}

	// create drones, all in warehouse 0
	for (int d = 0; d < ds.nbDrones; ++d)
	{
		Drone dr;
		dr.id = d;
		dr.cell = Warehouses[0].cell;
		dr.time = 0;
		Drones.push_back(dr);
	}
	
	
	// order
	long nbOrders;
	in >> nbOrders;
	
	for (int n = 0; n < nbOrders; ++n)
	{
		Order order;
		order.id = n;
		in >> order.cell.row;
		in >> order.cell.col;

		long nbp;
		in >> nbp;

		for (int p = 0; p < nbp; ++p)
		{
			long pin;
			in >> pin;
			order.items.push_back(pin);
		}
		
		Orders.push_back(order);
	}

	list<Command*> commands = ds.deliver();
	out << commands.size() << endl;

	for (Command *c : commands)
	{
		out << c->toString()<<"\n";
	}

	Sleep(15000);
	
}
